# Analytics

## Projektidé 

Mashupen kommer att kombinera och använda datat från Google Analytics Api, Facebook FQL, Twitter Api. 
Tanken är att man som ägare av en sida som är kopplad till Google analytics, ska kunna skriva in en url och få fram data som visas tillsammans med sociala nyckeltal.  
  
  
Analytics api:t kommer att användas för att ta fram:  

* Besök
* Unik besök
* Sidvisningar
* Datakällor
* Plattform
> *Dessa kan komma att ändra*

Facebook och Twitters apie:r kommer att användas för att ta fram Shares, Likes, och tweets genom att kolla mot Url.

En ytterligare funktionalitet som jag har i åtanke är att använda Twitters sök, för att söka på url:en i klartext för att sedan hitta tweets, och sedan försöka hitta retweets och få en bild på hur stor massa människor man har nått. 

I och med att jag använder Google Analytics Api så kommer jag att behandla känslig data i form av en analytics kod, som är personlig för användaren. 
Jag kommer därför att behöva använda mig av en inloggningsmodul.

En tanke kan vara att man erbjuder en sökning på url utan inloggning, och då alltså utan analytics funktionalitet.

Får jag tid så ska jag försöka använda mig av en databas för att få en historik över gamla sökningar.

## API:er

* Google Analytics Api
* Facebook FQL
* Twitter api


## Teknik

Lutar åt PHP Laravel på server sidan och kanske något javascript ramverk på klientsidan. Dock har jag blivit lite frälst på javascript igen, så det kanske lutar mot testköra lite node.js igen.  
Chart.js har jag länge letat efter en god anledning att få prova så det är givet.

