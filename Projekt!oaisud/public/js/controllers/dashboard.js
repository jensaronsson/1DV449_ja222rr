/* global insights */
'use strict';

insights.controller('dashboard', function($scope, $http) {



    var items = null;
    $scope.$on('$routeChangeSuccess', function () {

        $http.get('/Analytics/profiles')
            .success(function(data) {
                items = data.items;
                $scope.profiles = data.items;
            });



    });

    $scope.fetchReport = function() {
        items.forEach(function(item, i) {
           if($scope.selectedProfile === item.id) {
               $scope.profile = item;
           }
        });

        $http.get('/Analytics/data?profile=' +  $scope.selectedProfile)
            .success(function(data) {

                var urls = [];
                data.rows.forEach(function(row, i) {
                    urls.push(
                    {
                        path: row[0],
                        visits: row[1]
                    });
                });

                $scope.results = {};
                $scope.social = null;
                $scope.results.paths = urls;
                $scope.results.totalVisits = data.totalsForAllResults['ga:visitors'];
            });


        $scope.checkSocialStatus = function() {

            $scope.results.fullSiteUrl = $scope.profile.websiteUrl + $scope.selectedPath.path;

            $http.get("/social?url=" + $scope.results.fullSiteUrl)
                .success(function(data) {
                    $scope.results.social = data;
                    $scope.resultData = true;
                });

        };
    };






});
