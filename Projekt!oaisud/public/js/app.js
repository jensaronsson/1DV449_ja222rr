/*global angular */
'use strict';
/*
* Insights Angular Model
*/

var insights = angular.module('insights', ['ngRoute','ngSanitize'])
    .factory('authService', function($q, $http, $location) {

        return  {
            isLoggedIn : function() {
                var defer = $q.defer();
                $http.get('/isLoggedIn')
                    .success(function(data) {
                        if(data.isLoggedIn) {
                            defer.resolve();
                        } else {
                            $location.path('/');
                        }
                    });

                return defer.promise;
            },
            logout : function() {
                var defer = $q.defer();
                $http.get('/logout')
                    .success(function(data) {
                        if(data) {
                            defer.resolve();
                        }
                    });
                $location.path('/');
                return defer.promise;
            }
        };
    })
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/',
            {
                controller : 'login',
                templateUrl : '/js/views/login.html'

            }).when('/logout', {
                controller : 'login',
                templateUrl : '/js/views/login.html',
                resolve : {
                    insights :  function(authService) {
                        return authService.logout();
                    }
                }

            }).when('/dashboard', {
                controller : 'dashboard',
                templateUrl : '/js/views/dashboard.html',
                resolve : {
                    insights : function(authService) {
                        return authService.isLoggedIn();
                    }
                }
            }).when('/settings', {
                controller : 'settings',
                templateUrl : '/js/views/settings.html'
            }).otherwise( { redirectTo : "/" } );

    });

