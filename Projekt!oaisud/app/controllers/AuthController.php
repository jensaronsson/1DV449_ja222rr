<?php

class AuthController extends BaseController {





    public function loginWithGoogle() {

        // get data from input
        $code = Input::get( 'code' );

        // get google service
        $googleService = OAuth::consumer( 'Google' );

        // check if code is valid

        // if code is provided get user data and sign in
        if ( !empty( $code ) ) {

            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken( $code );

            $accessToken = $token->getAccessToken();


            // Send a request with it
           // $result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);
            // TODO:: save profile to db.



            Session::put('Credentials',['Authenticated' => true, 'token' => $accessToken]);
            return Redirect::to('/#/dashboard');



        }
        // if not ask for permission first
        else {
            // get googleService authorization
            $url = $googleService->getAuthorizationUri();

           // return to google login url
           return Redirect::to((string)$url);
        }
    }

    public function isLoggedIn() {

        return Response::json(array('isLoggedIn' => Session::get('Credentials')['Authenticated']));
    }

    public function logout() {
        Session::forget('Credentials');
        \Log::info('Session cleared : ' . Session::get('Credentials'));
        return Response::json(true);
    }

}