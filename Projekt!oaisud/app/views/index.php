<!doctype html>
<html lang="sv">
<head>
    <meta charset="UTF-8">
    <title>Insights</title>
    <link rel="stylesheet" href="/css/main.css"/>
</head>
<body ng-app="insights">




<div class="container">
    <div ng-view>
    </div>
</div>


<script src="/bower_components/angular/angular.min.js"></script>
<script src="/bower_components/angular-route/angular-route.js"></script>
<script src="/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script src="/bower_components/angular-chartjs-directive/chartjs-directive.js"></script>
<script src="/bower_components/angular-chartjs-directive/lib/Chart.min.js"></script>
<script src="/js/app.js"></script>
<script src="/js/controllers/login.js"></script>
<script src="/js/controllers/dashboard.js"></script>
<script src="/js/controllers/settings.js"></script>



</body>
</html>