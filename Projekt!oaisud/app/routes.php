<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//use Gapi\Gapi;

Route::get('/', function() {
    return View::make('index');
});
Route::get('/oauth', 'AuthController@loginWithGoogle');
Route::get('/isLoggedIn', 'AuthController@isLoggedIn');
Route::get('/logout', 'AuthController@logout');
Route::group(array('before' => 'ajax'), function() {

    Route::resource('Insights', 'InsightsController');

    Route::resource('Analytics', 'AnalyticsController');
    Route::get('/social', 'SocialMediaController@get');


});





//Route::get('/test', function()
//{
//
//    $token = Session::get('token');
//    $gapi = new Gapi($token);
//    $data = $gapi->requestAccountData();
//
//
//
//    dd($data);
//
//
//    $d = $gapi->requestReportData($json_obj[3]['profileId'], array("hostname"), array("visits"), "-visits", null, "2012-01-01", "2014-01-06", 1, 100);
//
//    var_dump($d);
//
//
//});

