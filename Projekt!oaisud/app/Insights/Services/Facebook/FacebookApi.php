<?php namespace Insights\Services\Facebook;


use Illuminate\Support\Facades\Response;

class FacebookApi
{
    public function getCounts($url){
        return $this->objectMapper(file_get_contents("https://graph.facebook.com/fql?q=SELECT%20share_count,%20like_count%20FROM%20link_stat%20WHERE%20url=%27" . $url .  "%27"));
    }

    public function objectMapper($obj)
    {

       $obj = json_decode($obj);
       return array('shares' => $obj->data[0]->share_count, 'likes' => $obj->data[0]->like_count);

    }
}