<?php namespace Insights\Services\Facebook;


use Illuminate\Support\ServiceProvider;

class FacebookServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public
    function register()
    {
        $this->app->bind('facebook', function() {
            return new FacebookApi();
        });
    }
}