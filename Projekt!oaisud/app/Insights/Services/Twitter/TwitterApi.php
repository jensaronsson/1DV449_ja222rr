<?php namespace Insights\Services\Twitter;

use Illuminate\Support\Facades\Response;

class TwitterApi
{


    public function getCounts($url){
        return $this->objectMapper(file_get_contents("http://urls.api.twitter.com/1/urls/count.json?url=" . $url));
    }

    public function objectMapper($twitterObject)
    {
        $tw = json_decode($twitterObject);
        return array('count' => $tw->count);
    }


}