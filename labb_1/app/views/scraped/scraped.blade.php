@extends('master')
@section('content')


<div class="header">
    <ul>
        <li><a href="/scrape" class="button">Skrapa</a></li>
    </ul>
</div>

@foreach ($producers as $producer)
    <div class="producer">
        
        <h2>{{ $producer->name }}</h2>
        <h3>Skrapades senast {{ $producer->updated_at }} </h3>
            @if ($producer->status == 200)
                <ul>
                    <li><p>Status: <span class="status">{{$producer->status}}</span> </p>  </li>
                    <li><p>Producent id: {{ $producer->producersId }}</p></li>
                    <li><p>Ort: {{ $producer->city }}</p></li>

                    @if($producer->url)
                        <li><p>Hemsida: <a href="{{ $producer->url }}">{{ $producer->url }}</a> </li>
                    @else
                        <li><p>Hemsida: Ingen uppgift</a></li>
                    @endif
                    @if($producer->image)
                        <li><img src="{{ $producer->image }}"></li>
                    @else
                        <li><p>Bild: Ingen bild</p></li>    
                    @endif
                    
                </ul>
            @else
                <p> Status: {{$producer->status }} </p>
                <p>Länken är bruten</p>
            @endif

        
        
    </div>
@endforeach



@stop