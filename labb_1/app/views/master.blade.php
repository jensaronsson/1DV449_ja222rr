<!doctype html>
<html lang="sv">
<head>
    <meta charset="UTF-8">
    <title>Skrape.me</title>
    <link rel="stylesheet" href=" {{ URL::asset('assets/stylesheets/style.css') }} ">
</head>
<body>
    @yield('content')
</body>
</html>