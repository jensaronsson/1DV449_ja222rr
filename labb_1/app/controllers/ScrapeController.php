<?php

class ScrapeController extends BaseController {

	public function followLogin($url) {
		$ch = curl_init();

		$postData = array(
						'username' => 'admin',
						'password' => 'admin',
		);

		curl_setopt_array($ch, array(
					CURLOPT_URL => $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POST => true,
					CURLOPT_POSTFIELDS => $postData,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_COOKIESESSION => true,
    				CURLOPT_COOKIEJAR => base_path() . '/cookie.txt',
    				CURLOPT_COOKIEFILE => base_path() . '/cookie.txt'
		));

		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}

	public function followLink($url) {

		$ch = curl_init();

		curl_setopt_array($ch, array(
					CURLOPT_URL => $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_COOKIESESSION => true,
    				CURLOPT_COOKIEFILE => base_path() . '/cookie.txt'
		));

		$output = curl_exec($ch);
		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$curl = array('html' => $output, 'status' => $http_status);
		return $curl;
	}

	public function scrape()
	{

		$producers_links = [];
		$stringHTML = $this->followLogin("http://vhost3.lnu.se:20080/~1dv449/scrape/check.php");

		$htmlNode = new Htmldom($stringHTML);

		foreach ($htmlNode->find('td a') as $element) {

			preg_match('/_(\d{1,2}).php/', $element->href, $match);
			$html = $this->followLink("http://vhost3.lnu.se:20080/~1dv449/scrape/secure/$element->href");
			$parsed = new Htmldom();
			$parsed = $parsed->load($html['html']);
			$producers_links[$match[1]] = array('name' => $element, 'html' => $parsed, 'status' => $html['status']);

		}
		
		
		foreach ($producers_links as $id => $producer) {
			// Check if there is scraped data in db
			$result = DB::table('producers')->where('producersId', '=', $id)->get();

			if ($result) {
				$prod = Producer::where('producersId', '=', $id)->first();
			} else {
				$prod = new Producer;
			}
			
			//Find nodes
			$city  = $producer['html']->find('span.ort', 0);
			$url   = $producer['html']->find('div.hero-unit a', 0);
			$image = $producer['html']->find('div.hero-unit img', 0);

			$imgBasePath = "http://vhost3.lnu.se:20080/~1dv449/scrape/secure/";

			if(count($image) > 0) {
				$image = $imgBasePath . $image->src;
			}
			else {
				$image = null;	
			}
	
			preg_match('/^http:\/\/|www/', $url, $match);
			if (!$match) {
				$url = null;
			}

			$city = preg_replace("/Ort:\s?(.+)/i", "$1", $city);
			$prod->name = strip_tags($producer['name']);
			$prod->city = strip_tags($city);
			$prod->url  = strip_tags($url);
			$prod->producersId = $id;
			$prod->status = $producer['status'];
			$prod->image = $image;
			
			$prod->save();			
			
		}


		return Redirect::to('/');
	}


	public function scraped() {

		$producers	 = Producer::all();
		
		return View::make('scraped.scraped')->with('producers', $producers);
	}
	
}