<?php 

class Producer extends Eloquent {
    protected $fillable = array('producersId', 'name', 'url', 'city', 'status', 'image');
};