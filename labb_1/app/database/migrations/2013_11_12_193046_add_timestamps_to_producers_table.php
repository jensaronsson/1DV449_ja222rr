<?php

use Illuminate\Database\Migrations\Migration;

class AddTimestampsToProducersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('Producers', function($table) {
			$table->timestamps('created_at')->default('CURRENT_TIMESTAMP');
			$table->timestamps('updated_at')->default('CURRENT_TIMESTAMP');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Producers', function($table) {
			$table->drop(array('updated_at', 'created_at'));
		});
	}

}