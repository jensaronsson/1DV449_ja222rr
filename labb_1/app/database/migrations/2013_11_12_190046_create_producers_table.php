<?php

use Illuminate\Database\Migrations\Migration;

class CreateProducersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Producers', function($table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->string('producersId', 2);
			$table->string('url', 70);
			$table->string('city', 50);
			$table->string('status', 3);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Producers');
	}

}