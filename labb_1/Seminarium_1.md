# 1 - Webben & integritet

### Vad ser du för faror för den personliga integriteten i ett mer och mer webbaserat samhälle? Vad har individen för ansvar för sin egen personliga integritet ur ett webbperspektiv?

Ett problem är ungdomars användning av sociala medier och smartphones. 
Konsekvenserna av ett taget foto som sprids på instagram kan få oerhörda följder som jag inte tror att en ungdom alltid kan förutse.

Man bör också se till att utbilda barn i hur datat kan komma till att användas i framtiden, så att man blir medveten om vad som kan hända i framtiden.



### Vad fick du för tankar när du läste publikationen "Storebror på Facebook"? 

Jag visste inte i hur stor utsträckning man säljer data och blev förvånad i hur mycket myndigheterna nyttjar dessa för att ytterligare få en ingång till att driva in pengar, fatta beslut etc. Jag fick också en direkt känsla av att stänga ner vartenda konto jag har på internet, jag fick en paranoid känsla av att jag var iakttagen.


### Stora företag som t.ex. Google och Facebook samlar massor av information om våra aktiviteter på webben genom att vi använder deras tjänster. Vad ser du för problem med detta? Vad ser du för fördelar? Motivera dina svar?

I den goda världen så ser jag stora fördelar med hur datat kan användas. Jag är ett stort fan av teknik och saker som underlättar vardagen och att t.e.x google lär sig mer om mig och skapar smarta tjänster som underlättar för mig, så är det positivt.
Det uppenbara problemet är att vi inte lever i en hundra procentigt god värld, och att informationen som samlas in värderas i pengar. När pengar blandas in så kan många moraliska och etiska linjer sprängas och det kan få tråkiga följder.  


# 1.2 Fallstudie
## Hur skulle du, som student, se på att alla dina aktiviter på Coursepress sparas på ett mer omfattande sätt för oss som kursansvariga att analysera? Vilka problem och vilka fördelar finns? Vad är OK att spara? Vad är inte OK?

OM det finns det data värda att titta på och det skulle leda till att inlärningen och kommunikationen mellan lärare och student kunde förbättras avsevärt, så självklart så tycker jag man ska analysera. Men om det kommer till priset av studenternas integritet skulle inkräktas så finns det inte inte något som skulle kunna argumenteras för att man ändå skulle samla in data. 

Det som är ok att spara är hur man interagerar på sidan, att se hur studenten nyttjar de resurser som finns. 

Det som inte är ok att spara är saker som är signifikativt för en enskild individ.





# 2 - Webbskrapning

## Vilka problem ser du med webbskrapning. Både som webbplatsägare (informationsägare) och som webbutvecklare. Nämn minst tre stycken, motivera dina val.

1. Som webbutvecklare är du beroende av hur sidans struktur är uppbyggd, ska man bygga informations sidor så kan det snabbt bli många trasiga länkar. Och tidsödande och underhålla.
2. Att man som webbutvecklare skrapar någons copyright skyddade material, vilket kan få rättsliga följder
3. Att man som informationsägare publicerar känslig information med för dålig säkerhet, kan skrapas och den känsliga informationen läcker ut.


## När man som webbutvecklare skrapar en sida finns det vissa etikettsregler som man bör försöka följa. Kan du kommer på några sådana regler? 
* Man bör fråga om lov innan man skrapar runt på sidor och  slänger ihop en liten mashup på sin egen sida.
* Man bör också tänka sig för om hur ofta man ska skrapa en sida, så man inte bidrar negativt hos servern man skrapar.
* Man bör ta en koll på sidans robots.txt för att se vad sidan har för attityd gentemot icke mänskliga besök.







 









