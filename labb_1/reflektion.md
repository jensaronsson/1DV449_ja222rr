### 1. Ni är fria att välja sätt att läsa in och extrahera data ur webbsidorna. Motivera ditt val!
Jag valde att använda en curl tillsammans med en html dom parser, då kan man använda likt css selectors för att hitta datat. Det blev väldigt smidigt.
### 2. Vad finns det för risker med applikationer som innefattar automatisk skrapning av webbsidor? Nämn minst tre stycken!
* Risken finns att man skicka för många anrop och skapar negativa effekter hos datahållaren.
* Det är svårt att hålla koll på att datat man får hem är korrekt, det är svårt att få till en bred gardering i skrapandet av data.
* Problem med att strukturen av html är känslig för en skrapa.

### 3. Tänk dig att du skulle skrapa en sida gjord i ASP.NET WebForms. Vad för extra problem skulle man kunna få då?
Först o främst så tycker jag synd om stackarn som gjort en sida i Webforms.
Problem som skrapare skulle vara att webforms kan generera en djungel av markup, svårt att hitta struktur. 
Viewstate kan bli stor o segladdad.

### 4. Har du gjort något med din kod för att vara "en god webbskrapare" med avseende på hur du skrapar sidan?
Jag har försökt att skriva kod så jag endast gör så få request som möjligt. 
Jag hämtar sedan datat och puttar in i databasen och gör sedan all logik.
Jag skulle i andra fall, kontaktat ägaren och frågat om lov innan jag skrapade.

### 5. Vad har du lärt dig av denna uppgift?
Jag har breddat kunskaperna i cURL, mina kunskaper sträckte sig till terminalen för att göra en enkel request. Visste inte hur pass konfigurerbar cURL är. 
Jag har också snappat upp en del etik och moral gällande skrapning, tyckte det var oklart förut.