----------
# Seminarium 03 - Rapport

## Projektidé 

Min mashup kommer att rikta sig mot SEO marknaden och bygga på att en användare ska kunna följa url:er som är “anslutna” till Google analytics. 
Denna data ska sedan presenteras tillsammans med data från Facebook och Twitter. 
Från Facebooks api kan man hämta antal shares från url:en och antal Likes. Twitters api hämtar man antal tweets, och även söker på url:en ifall någon har twittrat och följer sedan upp och kollar antal retweets och ser den totala mängden människor den nått ut.

Datat som ska presenteras är:

* Besök
* Unika besök
* Sidvisningar
* Datakällor
* Plattform
* Webbläsare
* Sociala delningar
* Facebook Likes
* Tweets
* Retweets av tweet med omnänd url

Eftersom applikationen kommer att hantera viss känslig data i form av en personlig token nyckel från google analytics konto så kommer det behövas en inloggninsmodul.

## API:er

* [Google Analytics](https://developers.google.com/analytics/)
* [Facebook](https://developers.facebook.com/)
* [Twitter](https://dev.twitter.com/) 

## Reflektion om API:er
* Google analytics api är som förväntat väldokumenterat men tycker det är oerhört svårt att komma igång och hitta vart man ska. Det är mycket text och svår namngivning så man vet inte riktigt vart man ska börja. 
Jag kommer använda mig av core reporting v3 api för att hämta ut data. Med core reporting kan man i princip hämta all analytisk data som ges på deras egna sida.

* Facebook api är väldokumenterat men tillskillnad från google är den väldigt lättnavigerad och hittar snabbt vad man vill ha. Jag kommer att använda mig av FQL api då det var enklast att få ut det jag behöver därifrån. FQL efterliknar SQL och gör det enkelt att få ut data.
 > * https://graph.facebook.com/fql?q=SELECT%20url,%20normalized_url,%20share_count,%20like_count,%20comment_count,%20total_count,commentsbox_count,%20comments_fbid,%20click_count%20FROM%20link_stat%20WHERE%20url=%27http://www.google.com%27


* Twitter api är väldokumenterat och lätt att navigera. Det finns tydliga listor på vilka anrop som kan göras, vilket jag tycker är positivt för att snabbt komma igång. Något jag inte lyckats hitta i Twitters dokumentation är att få ut antal tweets från en Url. Det verkar inte finnas stöd för detta. Men efter lite efterforskningar, så verkar det finnas en lösning genom att använda en url som används för tweet knappar. Detta är dock inget som stöds i API:t
> *exempel på knapp*
> 
> ![image](https://s3-us-west-2.amazonaws.com/droplr.storage/files/acc_14154/ddxx?AWSAccessKeyId=AKIAJSVQN3Z4K7MT5U2A&Expires=1387402967&Signature=JAYWyX9I2g%2B6KP9tcOd%2Bxdfm0mg%3D&response-content-disposition=inline%3B%20filename%3DScreen%2520Shot%25202013-12-18%2520at%252021.42.36.png%3B%20filename%2A%3DUTF-8%2527%2527Screen%2520Shot%25202013-12-18%2520at%252021.42.36.png)

## Dataformat

* Facebook: json, xml
* Twitter: json
* Google Analytics: json


## Begränsningar

* Google Analytics
> Är fritt att använda men till en viss begräsning (*antal förfrågningar per dag, profil.*)
> 
> * 10,000 förfrågningar. 
> * 10 samtidiga förfrågningar
>
> Skulle man överskrida dessa så kan man skicka en begära fler med motivering.

* Facebook
> Är fritt och fick leta in djup i deras policies för att hitta en generell begränsning på api förfrågningar. Även där går det att diskutera med facebook
> > 100M API calls per day
> 


* Twitter
> Är fritt att använda men har en begränsning på 350 förfrågningar per timme.


## Användarrisker

Då jag kommer att jobba mot Google Analytics API så kommer det krävas att man som användare av min tjänst har ett Analytics-konto sedan tidigare. Användare kommer alltså att skriva in autensieringsuppgifter mot google analytics från min tjänst och detta innebär att en användare måste känna ett stort förtroende för min tjänst innan de tar beslutet att prova min tjänst. Här finns det en stor risk med att folk inte vågar testa för att de känner sig inte bekväma och jag måste därför bygga upp en slags "trust" för att de ska lita på mig. Fördelen här är att jag jobbar mot Google som är ett stort namn och som alla känner till, och jag kan luta mig mot dom för öka trovärdighet. Det är av största vikt att jag får användarna att känna att detta är en säker och trygg tjänst.

## Tekniska risker

Den data jag kommer att samla in kommer som det ser ut nu, att komma från minst tre API:er; Facebook, Twitter och Google Analytics. Det är stora internetaktörer och deras API:er borde vara stabila och ge mig data utan några problem. Vad som skulle kunna ställa till problem är Twitters API, där man går via "tweet"-knappen för att få antalet tweets för en URL. Detta sätt att hämta ut data från Twitter är egentligen inget som stöds och därför finns de risk att de plötsligt stänger av möjligheten att få ut denna data. 
Det finns såklart risk för att de andra API:erna slutar att fungera. Det är därför viktigt att lägga vikt vid felhantering, att få låg koppling mellan de olika datakällorna så inte hela applikationen faller ifall ett av API:erna ändrar eller ligger nere.


# Fallstudie - Exempel på en bra befintlig mashup-applikation

En bra mashup som jag själv nyttjar flitigt är en mashup [http://www.bestofsvt.se]()
sidan är skapad av Ted Valentin och den kombinerar Facebook Api och Svt play (skrap).

Ted Valentin insåg att det fanns ca 3000 program på svtplay att titta på men endast en bråkdel av dessa va sevärda. Han tog då hjälp av folket på facebook och tog fram de mest delade svtplay klippen och fick på det viset fram en trovärdig topplista på bra kvalitetsprogram att titta på.

 ## Varför är denna applikation ett bra exempel på mashup-applikation?
 
* Detta är en otroligt smart idé och är enligt mig ett praktexempel på att visa vilken kraft en simpel mashup av två datakällor kan ge för effekt. 
Mervärdet av denna applikation att man som tittare enkelt kan sortera ut vilka klipp som är bra, för att istället chansa och spendera tid på att titta på eventuellt dåliga program, innan man kommer fram till ett bra program.


## På vilket sätt kombineras datakällorna och vilken ny effekt får dessa tillsammans?

Ted Valentin använder sig av Facebook Api för att ta reda på alla delade klipp från Svtplay.se. Han samlar dessa för att få en inlblick i hur pass populär klippet verkar vara. 
Han skrapar sedan klippen och presenterar och sorterar klippen som en topplista.

Effekten av detta är man stämplar varje klipp med en personlig referens. Som en kvalitetsstämpel av självaste svenska folket. 
Det blir lättare att avnjuta bra tv hemma i soffan. Även upphovsmakarna kan få en rejäl boost i upppskattning då det blir tydligt hur populärt klippen är. 



## Källor:

* Facebook Platform Policies, Dokumentation, 2013 [https://developers.facebook.com/policy/][1] (2013-12-18)
* Bestofsvt.se, mitt "hack" av SVT play, Ted Valentin, 2013, [http://www.tedvalentin.com/2013/01/bestofsvtse-ett-kul-hack-av-svt-play.html][2] (2013-12-18)
* REST API Rate Limiting in v1.1, Twitter Developers, 2013, [https://dev.twitter.com/docs/rate-limiting/1.1][3] (2013-12-18)
* Configuration and Reporting API Limits and Quotas, Google Developers, 2013 [https://developers.google.com/analytics/devguides/reporting/core/v3/limits-quotas][4] (2013-12-18)



  [1]: https://developers.facebook.com/policy/
  [2]: http://www.tedvalentin.com/2013/01/bestofsvtse-ett-kul-hack-av-svt-play.html
  [3]: https://dev.twitter.com/docs/rate-limiting/1.1
  [4]: https://developers.google.com/analytics/devguides/reporting/core/v3/limits-quotas