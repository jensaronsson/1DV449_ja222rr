(function (window) {
  'use strict';

    function GoogleMapsApi(long, lat) {
      this.map = null;
      this.writeMap(long, lat);
      this.markers = [];

    }

    Object.defineProperties(GoogleMapsApi.prototype, {

        writeMap : {
          value : function(long, lat) {

          var mapOptions = {
                center: new google.maps.LatLng(long, lat),
                zoom: 6,
              };
            this.map = new google.maps.Map(document.querySelector("#map-canvas"), mapOptions);


          }
        },
        writeMarkers : {
          value : function(trafficMessages) {
              var infoContent,
              marker;

              this.markers = [];

              for (var i = trafficMessages.length - 1; i >= 0; i--) {
                this.markers.push(marker = new google.maps.Marker({
                  position: new google.maps.LatLng(trafficMessages[i].latitude, trafficMessages[i].longitude),
                  map: this.map,
                  title: trafficMessages[i].subcategory
                }));

                var infoMarker = "<div class='infoBox'><span class='category'>" + trafficMessages[i].category + "</span> <span class='subcategory'>" + trafficMessages[i].subcategory + "</span>"+
                                  "<span class='meta'> <span class='date'>" +
                                  trafficMessages[i].createddate + "</span>" +
                                  "<div class='content'><p>" + trafficMessages[i].description + "</p></div></div>";

                var infoWindow = new google.maps.InfoWindow({
                  content : infoMarker,
                  maxWidth : 320

                 });
                this.setContent(marker, infoWindow);
              }
            }
        },
      clearMarkers : {
        value : function() {
          $.each(this.markers, function(i, marker) {
            marker.setMap(null);
          });
        }
      },
      setContent : {
        value : function(marker, infoWindow) {

            google.maps.event.addListener(marker, 'click', function() {
              infoWindow.open(this.map, marker);
            });
        }
      }
    });


  window.Traffic = window.Traffic || {};
  window.Traffic.Libs = window.Traffic.Libs || {};
  window.Traffic.Libs.GoogleMapsApi = GoogleMapsApi;
})(window);
