(function (window) {
    'use strict';

    function ApiService(config) {
        config           = config || {};
        config.size      = config.size || 100;
        config.cacheTime = config.cacheTime || 3000;
        this.config      = config;
        this.baseUrl     = "http://api.sr.se/api/v2/traffic/messages?format=json&size=100";

        Object.defineProperties(this, {
            CacheIsOld : {
                get : function() {
                    if (localStorage['lastCall']) {
                        return new Date().getTime() > +localStorage['lastCall'] + this.config.cacheTime;
                    } else {
                        return true;
                    }
                 }
            }
        });
    }

    Object.defineProperties(ApiService.prototype, {

        getData : {
            value : function(where, callback) {

                if (this.CacheIsOld) {

                    var messages = [];

                    $.getJSON( this.baseUrl + "&callback=?", function( data ) {

                        $.each(data.messages, function(i, message) {
                            messages.push(new window.Traffic.Model.Message(message));
                        });
                        this.saveToLocalStorage(messages);
                        callback(messages);

                    }.bind(this));

                } else {
                    this.getFromLocal(function(data) {
                        callback(data);
                    });
                }
            }

        },
        getFromLocal : {
            value : function(callback) {
                if (localStorage && localStorage['trafficMessages']) {
                    callback(JSON.parse(localStorage.getItem('trafficMessages')));
                }
            }
        },
        saveToLocalStorage : {
            value : function(data) {
                if (localStorage) {
                    localStorage.setItem('lastCall', new Date().getTime() + this.config.cacheTime);
                    localStorage.setItem('trafficMessages', JSON.stringify(data));
                }
            }
        },
    });

    window.Traffic = window.Traffic || {};
    window.Traffic.Model = window.Traffic.Model || {};
    window.Traffic.Model.ApiService = ApiService;

})(window);