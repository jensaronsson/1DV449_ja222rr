(function () {
    'use strict';

    function Traffic() {
        this.map               = new window.Traffic.Libs.GoogleMapsApi(61.3167, 14.8333);
        this.service           = new window.Traffic.Model.ApiService({size: 100, cacheTime: 3000});
        this.trafficModel      = new window.Traffic.Model.TrafficModel(this.service);
        this.trafficView       = new window.Traffic.View.TrafficView();
        this.trafficController = new window.Traffic.Controller.TrafficController(this.trafficView, this.trafficModel, this.map);
    }

    var traffic = new Traffic();

    window.addEventListener('load', function() {
        traffic.trafficController.listTraffic("all");
    });


})();