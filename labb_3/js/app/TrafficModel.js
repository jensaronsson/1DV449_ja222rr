(function (window) {
    'use strict';

    function TrafficModel(service) {
        this.service = service;
        this.category = "all";

    }

    Object.defineProperties(TrafficModel.prototype, {
        // Ugly callback hell.
        getData : {
            value : function(where, callback) {




                this.service.getData(where, function(data) {

                    if (this.category === "all") {
                        callback(data);
                    } else {
                        this.filterCategories(data, function filteredData(data) {
                            callback(data);
                        });
                    }
                }.bind(this));
            }
        },
        getPages : {
            value : function() {
                return this.service.getPages();
            }
        },
        setCategory : {
            value : function(category) {
                this.category = category;
            }
        },
        filterCategories : {
            value : function(data, callback) {
                var messages =
                    data.filter(function(data) {
                        return data.category === this.category;
                    }.bind(this));
                callback(messages);
            }
        }
    });




    window.Traffic = window.Traffic || {};
    window.Traffic.Model = window.Traffic.Model || {};
    window.Traffic.Model.TrafficModel = TrafficModel;

})(window);