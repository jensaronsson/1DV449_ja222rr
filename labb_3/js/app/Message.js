(function (window) {
    'use strict';


    function Message(message) {
        this.id          = message.id;
        this.title       = message.title;
        this.description = message.description;
        this.subcategory = message.subcategory;
        this.category    = message.category;
        this.createddate = message.createddate;
        this.latitude    = message.latitude;
        this.longitude   = message.longitude;
        this.priority    = message.priority;
        this.parseDate();
        this.parseCategory();

    }

    Object.defineProperties(Message.prototype, {

        parseDate : {
            value : function() {
                var match = this.createddate.match(/(\d+)\+(\d{4})/);
                var date = new Date(+match[1]);
                this.createddate = date.getFullYear() + " / ";
                this.createddate += date.getMonth() + " / ";
                this.createddate += date.getUTCDate() + " - ";
                this.createddate += date.getUTCHours() + ":";
                this.createddate += date.getUTCMinutes();
            }
        },
        parseCategory : {
            value : function() {
                var categoriesArray = ["Vägtrafik", "Kollektivtrafik","Planerad störning", "Övrigt"];
                this.category = categoriesArray[this.category];
            }
        }
    });



    window.Traffic = window.Traffic || {};
    window.Traffic.Model = window.Traffic.Model || {};
    window.Traffic.Model.Message = Message;
})(window);