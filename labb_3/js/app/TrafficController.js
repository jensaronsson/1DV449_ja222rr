(function (window) {
    'use strict';

    function TrafficController(view, model, map) {
        this.model    = model;
        this.view     = view;
        this.map      = map;


        // Pagination, not finished.
        this.view.bind('nextPage', function() {
            this.listTraffic('next');
        }.bind(this));
        // Pagination, not finished.
        this.view.bind('prevPage', function() {
            this.listTraffic('prev');
        }.bind(this));

        this.view.bind('categoryChange', function(el) {
            this.model.setCategory(el.target.attributes[0].value);
            this.listTraffic('all');
        }.bind(this));

    }

    Object.defineProperties(TrafficController.prototype, {

        listTraffic : {
            value : function(where) {
                this.model.getData(where, function(data) {
                    this.view.render('listMessages', data);
                    this.map.clearMarkers();
                    this.map.writeMarkers(data);
                }.bind(this));

            }
        }
    });

    TrafficController.prototype.getPage = function(where, el) {
        this.model.getData(where, function(data) {
            this.view.render('listMessages', data);
            this.map.clearMarkers();
            this.map.writeMarkers(data);
        }.bind(this));
    };

    window.Traffic = window.Traffic || {};
    window.Traffic.Controller = window.Traffic.Controller || {};
    window.Traffic.Controller.TrafficController = TrafficController;

})(window);