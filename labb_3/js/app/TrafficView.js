(function (window, $) {
    'use strict';

    function TrafficView() {
        this.trafficList     = $('div.list');
        this.categoryButtons = $("li.category");
        this.ulPagination    = $('ul.pagination');
    }


    Object.defineProperties(TrafficView.prototype, {

        render : {
            value: function(action, data) {
                var that = this;
                var actions = {
                    listMessages : function() {
                        that.trafficList.empty();

                        var div = "";
                        $.each(data, function(i, message) {


                             div += "<div class='trafficMessage'><h2>" + message.category  + " - <span class='subcategory'>" + message.subcategory + "</span></h2>" +
                                    "<span class='meta'>" + message.createddate + "</span>" +
                                    "<p class='message'>" + message.description  + "</p></div>";
                        });
                        that.trafficList.append(div);

                    },
                    listPages : function() {
                        var linkList = "";
                        for (var i = 1; i < data.pages; i++) {
                            linkList += "<li><a>" +  i + "</a></li>";
                        }

                        this.ulPagination.append(linkList);

                    }
                };

                actions[action]();
            }

        },
        bind : {
            value : function(el, callback) {
                if (el === "nextPage") {
                    $("a#nextPage").on('click', function(e) {
                        callback(e);
                    });
                }
                if (el === "prevPage") {
                  $("a#prevPage").on('click', function(e) {
                        callback(e);
                    });
                }
                if (el === "categoryChange") {
                    for (var i = 0; i < this.categoryButtons.length; i++) {
                        this.categoryButtons[i].addEventListener('click', function(e) {
                            callback(e);
                        });
                    }
                }
            }
        }
    });

    window.Traffic = window.Traffic || {};
    window.Traffic.View = window.Traffic.View || {};
    window.Traffic.View.TrafficView = TrafficView;

})(window, $);