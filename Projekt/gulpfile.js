var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    csso = require('gulp-csso'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat');


gulp.task('css', function () {
    gulp.src('public/css/*.scss')
        .pipe(csso())
        .pipe(concat('main.css'))
        .pipe(gulp.dest('public/dist/css'));

});

gulp.task('js', function () {
    gulp.src(['public/js/**/*.js', 'public/*.js'])
        .pipe(uglify())
        .pipe(concat('all.js'))
        .pipe(gulp.dest('public/js/dist'));
});


gulp.task('default', function () {
    gulp.run('css', 'js');
//    gulp.watch('./_includes/css/*', function () {
//        gulp.run('css');
//    });
//
//    gulp.watch('./_includes/js/*', function () {
//        gulp.run('js');
//    });
});

//// Include gulp
//var gulp = require('gulp');
//
//// Include Our Plugins
//var sass = require('gulp-sass');
//
//var lr = require('tiny-lr'),
//    refresh = require('gulp-livereload'),
//    server = lr();
//
//
//// Task SASS
//gulp.task('sass', function() {
//    gulp.src([
//        'public/css/*.scss'
//        ])
//        .pipe(sass({
//            includePaths: ['scss']
//        }))
//        .pipe(gulp.dest('public/css'))
//        .pipe(refresh(server));
//});
//
//
//gulp.task('refresh', function() {
//    gulp.src([
//        'public/js/views/*.html',
//        'app/views/*.php'
//        ])
//        .pipe(refresh(server));
//});
//
//
//// Task: default
//gulp.task('default', function() {
//    gulp.run('sass');
//
//    server.listen(35729, function (error) {
//        if (error) return console.log(error);
//
//        gulp.watch([
//            'public/css/**',
//            'public/img/**'
//            ], function(event) {
//            gulp.run('sass');
//        });
//
//        gulp.watch([
//            'public/js/views/*.html',
//            'app/views/*.php'
//            ], function(event) {
//            gulp.run('refresh');
//        });
//    });
//});