<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//use Gapi\Gapi;

Route::get('/', function() {
    return View::make('index');
});
Route::get('/oauth', 'AuthController@loginWithGoogle');
Route::get('/isLoggedIn', 'AuthController@isLoggedIn');

Route::group(array('before' => 'auth'), function() {

    Route::get('/social', 'SocialMediaController@get');
    Route::get('/logout', 'AuthController@logout');
    Route::get('/Analytics/profiles', 'AnalyticsController@profiles');
    Route::get('/Analytics/visits/{profileid}/{dateFrom}/{dateTo}', 'AnalyticsController@visits')
        ->where(array('dateFrom' => '[0-9]{4}-[0-9]{2}-[0-9]{2}|[30]+(daysAgo)', 'dateTo' => '[0-9]{4}-[0-9]{2}-[0-9]{2}|today'))
        ->before('checkDate|checkCache')
        ->after('saveToCache');
    Route::get('/Analytics/source/{profileid}/{dateFrom}/{dateTo}', 'AnalyticsController@source')
        ->where(array('dateFrom' => '[0-9]{4}-[0-9]{2}-[0-9]{2}|[30]+(daysAgo)', 'dateTo' => '[0-9]{4}-[0-9]{2}-[0-9]{2}|today'))
        ->before('checkDate|checkCache')
        ->after('saveToCache');
    Route::get('/Analytics/refresh', 'AuthController@refreshAuthentication');

});





