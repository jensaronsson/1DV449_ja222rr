<?php namespace Insights\Services\Google;


use Illuminate\Support\Facades\Facade;

class Google extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'google';
    }
}