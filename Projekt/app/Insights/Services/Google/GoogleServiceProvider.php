<?php namespace Insights\Services\Google;


use Illuminate\Support\ServiceProvider;

class GoogleServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
       $this->app->bind('google', function() {
           return new GoogleApi();
       });
    }
}