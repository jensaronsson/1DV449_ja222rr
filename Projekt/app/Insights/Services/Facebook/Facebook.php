<?php namespace Insights\Services\Facebook;

use Illuminate\Support\Facades\Facade;

class Facebook extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'facebook';
    }
}