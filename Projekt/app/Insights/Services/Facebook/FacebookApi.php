<?php namespace Insights\Services\Facebook;


use Exception;
use Insights\Services\HttpClient\HttpClient;
use Illuminate\Support\Facades\Response;


class FacebookApi
{
    public function getCounts($url){

        $response = HttpClient::get("https://graph.facebook.com/fql?q=SELECT%20share_count,%20like_count%20FROM%20link_stat%20WHERE%20url=%27" . $url .  "%27");

        return $this->objectMapper($response);

    }

    public function objectMapper($obj)
    {

        $data = json_decode($obj['data']);

        $likes  =   isset($data->data[0]->like_count) ? $data->data[0]->like_count : null;
        $shares =   isset($data->data[0]->share_count) ? $data->data[0]->share_count : null;



        return array('shares' => $shares, 'likes' => $likes, 'total' =>  $likes, 'Error' => $obj['error'], 'statuscode' =>$obj['statuscode']);

    }
}