<?php namespace Insights\Services\Pinterest;


use Illuminate\Support\Facades\Response;

class PinterestApi
{
    public function getCounts($url){
        return $this->objectMapper(file_get_contents("http://widgets.pinterest.com/v1/urls/count.json?source=1&url=" . $url));
    }

    public function objectMapper($obj)
    {
       return array('count' => $obj);
    }
}