<?php namespace Insights\Services\Pinterest;


use Illuminate\Support\ServiceProvider;

class PinterestServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public
    function register()
    {
        $this->app->bind('pinterest', function() {
            return new PinterestApi();
        });
    }
}