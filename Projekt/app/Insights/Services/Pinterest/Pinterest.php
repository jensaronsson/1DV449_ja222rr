<?php namespace Insights\Services\Pinterest;

use Illuminate\Support\Facades\Facade;

class Pinterest extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'pinterest';
    }
}