<?php namespace Insights\Services\HttpClient;

use Illuminate\Support\ServiceProvider;

class HttpClientServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public
    function register()
    {
        $this->app->bind('httpclient', function() {
            return new HttpClientApi();
        });
    }


}