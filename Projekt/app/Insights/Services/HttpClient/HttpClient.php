<?php namespace Insights\Services\HttpClient;

use Illuminate\Support\Facades\Facade;


class HttpClient extends Facade {

    public static function getFacadeAccessor()
    {
        return 'httpclient';
    }

}