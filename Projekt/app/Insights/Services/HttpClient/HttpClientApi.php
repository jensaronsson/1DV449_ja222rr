<?php namespace Insights\Services\HttpClient;

class HttpClientApi {

    private $ch;

    public function get($url) {

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($this->ch, CURLOPT_URL, $url);

        $data = curl_exec($this->ch);

        $error = curl_error($this->ch);
        $error === "" ? $error = false : $error = true;
        $http_status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);


        curl_close($this->ch);

        return array("data" => $data, "error" => $error, "statuscode" => $http_status);
    }


}