<?php namespace Insights\Services\Twitter;


use Illuminate\Support\ServiceProvider;

class TwitterServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('twitter', function() {
            return new TwitterApi();
        });
    }
}