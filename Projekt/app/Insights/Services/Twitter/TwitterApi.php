<?php namespace Insights\Services\Twitter;

use Illuminate\Support\Facades\Response;
use Insights\Services\HttpClient\HttpClient;

class TwitterApi
{


    public function getCounts($url){
        return $this->objectMapper(HttpClient::get("http://urls.api.twitter.com/1/urls/count.json?url=" . $url));
    }

    public function objectMapper($twitterObject)
    {
        $tw = json_decode($twitterObject['data']);
        $count = isset($tw->count) ? $tw->count : null;
        return array('likes' => $count, 'Error' => $twitterObject['error'], 'statuscode' => $twitterObject['statuscode'] );
    }


}