<?php


class User extends Eloquent {

    protected $fillable = array('profileId', 'refreshToken');
}