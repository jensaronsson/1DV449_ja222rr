<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});


/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/
Route::filter('auth', function()
{
    $auth = Session::get('oauth_token')['Google'];
    if(is_null($auth)) return Response::json(array('error' => array('status' => 403, 'response' =>'This route need\'s authorization')));
});

Route::filter('checkDate', function($route, $request    )
{
    $comparedate = strtotime("2005-01-01");
    $from   = strtotime($route->parameter('dateFrom'));
    $to     = strtotime($route->parameter('dateTo'));

    if($to < $from) {
        return Response::json(array('error' => array('status' => 898, 'message' => "From date preceeds to date.")));
    }

    if($comparedate > $from || $comparedate > $to) {
       return Response::json(array('error' => array('status' => 899, 'message' => "Date to far back. Analytics didn't exists before 2005-01-01")));
    }

});

Route::filter('checkCache', function($route)
{
    $action = $route->getActionName();
    $action = preg_match("/@([a-z]+)/",$action, $match);
    $id     = $route->parameter('profileid');
	$to     = $route->parameter('dateTo');
    $from   = $route->parameter('dateFrom');
    $key    = $match[1] . "/" . $id . "/" . $to . "/" . $from;

    if(Cache::has($key)) {
        $value = Cache::get('key');
        return $value;
    }

});
Route::filter('saveToCache', function($route, $request, $response)
{

    $action = $route->getActionName();
    $action = preg_match("/@([a-z]+)/",$action, $match);
    $id     = $route->parameter('profileid');
    $to     = $route->parameter('dateTo');
    $from   = $route->parameter('dateFrom');
    $key    = $match[1] . "/" . $id . "/" . $to . "/" . $from;

    if( ! Cache::has($key)) {
      Cache::put($key, $response->getContent(), 15 );
    };


});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});