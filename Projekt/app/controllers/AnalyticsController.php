<?php

use Gapi\Gapi;

class AnalyticsController extends BaseController
{


    const maxresult = 1000;

    public function profiles()
	{
        $gapi = App::make('Gapi');
        try {
            $data = $gapi->requestAccountData();
        }
        catch(RuntimeException $e) {

            AuthController::refreshAuthentication();
            $data = $gapi->requestAccountData();
        }
        return Response::json($data);

	}

    public function visits($profileId, $dateFrom, $dateTo)
    {
        return
            $this->requestGapi(array($profileId, null, array("visits","visitors", "pageviews"), null, null, $dateFrom, $dateTo, 1, self::maxresult));
    }

    public function source($profileId, $dateFrom, $dateTo)
    {
        return
            $this->requestGapi(array($profileId, array("source"), array("visitors"), "-visitors", null, $dateFrom, $dateTo, 1, self::maxresult));

    }
    public function profile($profileId, $dateFrom, $dateTo)
    {
        return
        $this->requestGapi(array($profileId, array("pagePath"), array("visitors"), null, null, $dateFrom, $dateTo, 1, self::maxresult));

    }

    public function requestGapi(Array $p) {
        $gapi = App::make('Gapi');
        try {
           return $gapi->requestReportData($p[0],$p[1],$p[2],$p[3],$p[4],$p[5],$p[6],$p[7],$p[8]);
        }
        catch(RuntimeException $e) {
            AuthController::refreshAuthentication();
            return $gapi->requestReportData($p[0],$p[1],$p[2],$p[3],$p[4],$p[5],$p[6],$p[7],$p[8]);
        }
    }




}