<?php

use Insights\Services\Exception\WebServiceFailException;
use Insights\Services\Facebook\Facebook;
use Insights\Services\Twitter\Twitter;

class SocialMediaController extends BaseController {

	public function get()
	{
        $url = Input::get('url');

        $fb = Facebook::getCounts($url);

        $tw = Twitter::getCounts($url);

        return Response::json(array('facebook' => $fb, 'twitter' => $tw, 'total' => $fb['total'] + $tw['likes'], 'Error' => array('facebook' => $fb['Error'], 'twitter' => $tw['Error'])));
	}



}