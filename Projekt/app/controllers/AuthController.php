<?php

class AuthController extends BaseController {

    public function loginWithGoogle() {
        $code = Input::get( 'code' );
        $googleService = OAuth::consumer('Google');

        // If code is empty then we redirect to google login page.
        if ( empty( $code ) ) {

            $url = $googleService->getAuthorizationUri(array("access_type" => "offline"));

            return Redirect::to((string)$url);

        }

        // Google has logged you in, request token.
        $accessToken = $googleService->requestAccessToken( $code );
        // Send a request with it
        $result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);



        $this->storeUser(array('profileId' => $result['id'], 'refreshToken' => $accessToken->getRefreshToken() !== null ? $accessToken->getRefreshToken() : "none" ));

        Session::put('userprofile',
            array('profileId' => $result['id'],'name' => $result['name'],
                'picture' => isset($result['picture'])
                ? $result['picture'] : null));

        return Redirect::to('/#/dashboard');
    }




    public function storeUser($user) {
        $userDb = DB::table('users')->where('profileId', $user['profileId'])->first();
        if(is_null($userDb)) {
            return User::create($user);
        }
        return $userDb;
    }


    public static function refreshAuthentication() {
        $googleService = OAuth::consumer('Google');
        $token= Session::get('oauth_token')['Google'];
        $user = Session::get('userprofile');
        $user = DB::table('users')->where('profileId', $user['profileId'])->first();

        $token->setRefreshToken($user->refreshToken);
        $googleService->refreshAccessToken($token);
        Log::info('Refreshed token!');
        return $token;
    }

    public function isLoggedIn() {
        $auth = Session::get('oauth_token')['Google'];
        return Response::json(array('isLoggedIn' => (!is_null($auth)), 'userprofile' => Session::get('userprofile')));
    }

    public function logout() {
        Session::forget('oauth_token');
        Session::forget('userprofile');
        return Response::json(true);
    }

}