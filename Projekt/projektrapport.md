# Rapport projekt webbteknik 2 - HT13



## Inledning

Jag har byggt en applikation som binder samman data från kopplade Google Analytics-konton och facebook, twitter likes/tweets. 
Ideén kommer från ett behov av personer som använder google analytics i arbetet med behovet att samla facebook/tweets på samma sida som basstatistik.

## Schematisk bild
[Länk till schema](http://cl.ly/image/181k442C0H2z)

## Serversida

Jag använder mig utav PHP ramverket [laravel](http://laravel.io) på serversidan för att bygga proxy ett api som klientet ska prata mot. 
Hanteringen av autensiering sker via oAuth och Google och det som sparas på användaren är id och en refreshtoken. När användaren väl är inloggad sätts en session och man är då inloggad livslängden av den öppna "fliken". 
Du blir också autensierad mot Google Analytics med hjälp av en Accesstoken som man måste skicka med vid varje anrop. Denna token är giltig i 120 minuter och efter detta så måste man omautensiera sig. 
Eftersom användaren ska slippa logga in med Google kontot gång på gång, så har min app krav på offline behörigheter på Analytics kontot. Detta gör att jag får en RefreshToken som kan användas för att förnya Accesstoken på nytt. 
På så sätt blir användaren aldrig utloggad från analytics förräns man loggar ut från klienten.


Jag har byggt Webbservice wrappers för de sociala mediernas api:er som är löst kopplade för att förhindra att hela applikationen kraschar vid eventuellt fel på ett av api:erna. 

Jag har använt mig av ett färdigt bibliotek till Analytics för att hämta ut rapporter, dock fick jag modifera det en hel del, då det ej var uppdaterat på länge. Detta var det bästa alternativet.

Felhanteringen tas hand om på servern som skickar felkoder ner till klienten. 
Alla anrop till servern måste gå via ett filter som ansvarar för att man är inloggad.


Jag cachar datat från Analytics i 15 minuter på servern, det sparas med en nyckel som är en kombination av profilid / controller / fråndatum / tilldatum





## Klientsida

På klienten har jag valt att använda javascript ramverket Angularjs. Där jag använder data-bindings och routing funktionalitet.
När en route triggas, alltså en länk som efterfrågas så går det automatisk en koll mot servern ifall användaren är inloggad, är man det så går man vidare till huvudsidan där all data presenteras, annars så åker man tillbaka till loginsidan.
Väl på huvudsidan så hämtas jag alla profiler/websidor som användaren har på 
sitt Google analytics konto. Användaren väljer datum och profil och det skickas förfrågan mot servern efter vald data. Finns det ingen data, tas detta hand om och presenteras för användaren.


Jag har ingen cachning av data på klientsidan pga av tidsbrist.
Men min tanke är att implementera detta, och då cachat data från de social medierna, då dessa är ej inom något datumspann, och är oftast ej så extremt föränderligt.


## Reflektion

Jag tycker projektet har gått svajigt. Jag tog för lätt på att både sätta mig in angular och laravel som jag inte jobbat med förut. Dock så kan jag känna i efterhand så har det hjälpt mig mycket med avancerade funktioner som har varit tacksamt. 
Men om jag skulle backat tillbaka tiden så hade jag lagt ribban lite lägre.
Jag har stött på många problem, och ett som tog tid var Google analytics api:et. Deras egna bibliotek var utdaterade och det fanns inget riktigt bra alternativ, utan fick tillslut bygga om ett befintligt som ej var uppdaterat på ett tag. Detta medförde att mycket tid försvann. 

Sedan har det funnit många småfel som varit till grund för att jag inte kunde något om ramverken jag valde. 

Jag satte mig också in i d3 svg-grafer vilket var intressant, men tog lite tid. 

Jag hade absolut velat fortsätta utveckla denna idé då jag inte hann med en funktionalitet som gärna vill utveckla. Det är att ta reda på hur många tweets som har webside url i text och sedan räkna ut på retweets på hur stor mängd människor man har nått ut till.



## Risker 

Det finns risker med min applikation eftersom jag använder mig av api:er, dock har dom minimerats genom att de inte är beroende av varandra.
Men med facebook och twitter så finns det en risk att dessa sluta fungera, då jag använder mig av en url som används av Facebook och Twitter knappar som kan användas på hemsidor för att visa hur många likes dessa har.
Dessa url:erna kräver ingen autensiering. 
Men skulle dessa sluta fungera så fungerar fortfarande min applikation. 

Såklart finns alltid risken med att ett api beslutar sig för att skicka data i annan struktur och då funkar det helt plötsligt inte med hur jag tar hand om datat. Men jag har byggt applikationen så det inte ska fallera hela applikationen.


Det finns en säkerhetsaspekt att beakta som användare och det är att jag som upphovsmakare har inte programmerat så att den skyddade datan är oläsbar för mig. Jag cachar datat och sparar undan i databasen, det är hashat men det är ingen garanti för att jag inte kan bli hackad och läcka data.
Detta speglar samtidigt den etiska biten med mig som upphovsman att vara tydlig med att jag inte använder datat som sparas undan.

Av min bedöming så har jag inga tydliga säkerhetshål som jag inte är medveten om. Jag kan nog dock optimera säkerheten på klientsidan något. Men för detta projekt anser jag det vara bra nog.



