/* global insights */
/* global angular */
'use strict';

insights.controller('dashboard', ['$scope', '$http', '$route', 'donutChart', '$q', function($scope, $http, $route, donutChart, $q) {
    $scope.userprofile = $route.current.locals.insights;
    $scope.results = {};
    $scope.profilePicker = true;
    $scope.isVisible = false;

    $scope.settingsToggler = function() {
        $scope.isVisible = !$scope.isVisible;
        $('.settings').slideToggle('slow');
    };

    $scope.clearData = function() {
        var svgs = d3.selectAll("svg");
        if(svgs !== null) {
            svgs[0][0].remove();
            svgs[0][1].remove();
        }
    };


    var items = null;
    $scope.$on('$routeChangeSuccess', function () {

        $http.get('/Analytics/profiles')
            .success(function(data) {
                items = data.items;
                $scope.profiles = data.items;

            });


    });

    $scope.fetchReports = function() {
        $scope.settingsToggler();
        items.forEach(function(item, i) {
           // Get id of the selected profile.
           if($scope.selectedProfile === item.id) {
               $scope.profile = item; // Set profileitem to profile scope
               $scope.profilePicker = false;
               $scope.dateFrom = $scope.dateFrom || "30daysAgo";
               $scope.dateTo = $scope.dateTo || "today";
               $scope.analyticsUrl = $scope.selectedProfile + "/" + $scope.dateFrom + "/" + $scope.dateTo;
               $scope.clearData();
               $scope.loading = true;
               getData().then(function() {

                $scope.loading = false;
               });
           }
        });

    };


    var getData = function() {
        var deferred = $q.defer();
        var done = 0;
        function isDone() {
            done++;
            if (done===3) {deferred.resolve();}
        }
        $http.get("/Analytics/visits/" +  $scope.analyticsUrl)
            .success(function(data) {

                if (typeof data.error !== 'undefined') {
                    if(data.error.status && data.error.status === 403) {
                        $scope.sessionTimeOut = true;
                        $scope.loading = false;
                    }
                    if(data.error.status === 899) {
                        $scope.responseError = true;
                        $scope.errorMsg = data.error.message;
                        $scope.loading = false;
                    }
                    if(data.error.status === 898) {
                        $scope.responseError = true;
                        $scope.errorMsg = data.error.message;
                        $scope.loading = false;
                    }
                }
                $scope.visits       = data.totalsForAllResults['ga:visits'];
                $scope.visitors     = data.totalsForAllResults['ga:visitors'];
                $scope.pageViews    = data.totalsForAllResults['ga:pageviews'];
                isDone();
            });
        // get social data; twitter, facebook.
        $http.get("/social?url=" +  $scope.profile.websiteUrl)

            .success(function(data) {

                $scope.errorShow = false;
                $scope.errorList = data.Error;
                $scope.errors = function(errors) {

                    var result = [];
                    angular.forEach(errors, function(error, i) {
                        if(error === true) {result.push(i);}
                    });

                    if(result.length > 0) {$scope.errorShow = true;}
                    return result;
                };
                delete data.Error;
                var donut = donutChart();

                $scope.social = donut.social(data);
                $scope.getY = donut.getY();
                $scope.getX = donut.getX();
                $scope.getColor = donut.getColor();
                isDone();
            });

        $http.get('/Analytics/source/' + $scope.analyticsUrl)
            .success(function(data) {

                if (typeof data.error !== 'undefined') {
                    if(data.error.status && data.error.status === 403) {
                        $scope.sessionTimeOut = true;
                        $scope.loading = false;
                    }
                    if(data.error.status === 899) {
                        $scope.responseError = true;
                        $scope.errorMsg = data.error.message;
                        $scope.loading = false;
                    }
                    if(data.error.status === 898) {
                        $scope.responseError = true;
                        $scope.errorMsg = data.error.message;
                        $scope.loading = false;
                    }
                }
                var donut = donutChart();
                $scope.source = donut.source(data);
                $scope.getY = donut.getY();
                $scope.getX = donut.getX();
                $scope.getColor = donut.getColor();
                isDone();

        });

        return deferred.promise;
    };

}]);
