/* global insights */

'use strict';
insights.controller('login', ['$scope', '$http', function($scope, $http) {
    $scope.analyticsApiKey = function() {
        $http({
            method: 'POST',
            url:  '/oauth',
            data: {
                username: $scope.username,
                password: $scope.password
            }
        }).success(function(data) {
                $scope.profiles = data;
            });

        $scope.selectedProfile = null;
    };
}]);
