/*global angular */
'use strict';
/*
* Insights Angular Model
*/

var insights = angular.module('insights', ['ngRoute','ngSanitize', 'legendDirectives', 'ngAnimate', 'nvd3ChartDirectives', 'ui.date']);



insights.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/',
            {
                controller : 'login',
                templateUrl : '/js/views/login.html'

            }).when('/logout', {
                controller : 'login',
                templateUrl : '/js/views/login.html',
                resolve : {
                    insights :  function(authService) {
                        return authService.logout();
                    }
                }

            }).when('/dashboard', {
                controller : 'dashboard',
                templateUrl : '/js/views/dashboard.html',
                resolve : {
                    insights : function(authService) {
                        return authService.isLoggedIn();
                    }
                }
            }).when('/settings', {
                controller : 'settings',
                templateUrl : '/js/views/settings.html'
            }).otherwise( { redirectTo : "/" } );
    }]);

