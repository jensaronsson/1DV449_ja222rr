/* global insights */
/* global angular */
'use strict';
insights.factory('authService', ['$q', '$http', '$location', function($q, $http, $location) {

    return  {
        isLoggedIn : function() {
            var defer = $q.defer();

            $http.get('/isLoggedIn')
                .success(function(data) {

                    if(data.isLoggedIn) {
                        defer.resolve(data.userprofile);
                    } else {
                        $location.path('/');
                    }
                });

            return defer.promise;
        },
        logout : function() {
            var defer = $q.defer();
            $http.get('/logout')
                .success(function(data) {
                    if(data) {
                        defer.resolve();
                    }
                });
            $location.path('/');
            return defer.promise;
        }
    };
}]);
insights.service('donutChart',  function() {

    return  function() {

        return {

            social : function(data) {
                var donut = [];

                if(data.total === 0) {return donut;}

                delete data.total;



                angular.forEach(data, function(row, i) {
                    if(data[i] === null)
                    {
                        data[i] = {};
                        data[i].likes = 0;
                        data[i].source = i + "unavailable";

                    }
                    this.push(
                        {
                            key: i,
                            y: +data[i].likes
                        }
                    );

                }, donut);

                return donut;

            },
            source : function(data) {
                var donut = [];

                angular.forEach(data.rows, function(row, i) {
                    if(i >= 0 && i <= 3 ) {

                        this.push(
                            {
                                y: +row[1],
                                key: row[0]
                            }
                        );

                    }

                }, donut);

                return donut;

            },
            getX : function() {
                return function(d){
                    return d.key;
                };
            },
            getY : function() {
                return function(d){
                    return d.y;
                };
            },
            getColor : function() {
                return function(d, i) {
                    var colors = [ "#40c1a0", "#81cfe0", "#195270", "#ffcad1", "#449bc8", "#F7464A", "#E2EAE9", "#D4CCC5"];
                    return colors[i];
                }.bind(this);
            }
        };
    };

});
