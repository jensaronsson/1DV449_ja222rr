<?php

/**
* Called from AJAX to add stuff to DB
*/
function addToDB($name, $message, $pid) {
	$db = null;
	
	try {
		$db = new PDO("sqlite:db.db");
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOEception $e) {
		die("Something went wrong -> " .$e->getMessage());
	}
	
	$q = "INSERT INTO messages (message, name, pid) VALUES(:message, :name, :pid)";
	
	
	try {
		$stm = $db->prepare($q);
		$stm->execute(array(':message' => safehtml($message),
		 					':name' => safehtml($name),
		 					':pid' => safehtml($pid)));	
	}
	catch(PDOException $e) {
		die("Something went wrong -> " .$e->getMessage());
	}
}


function safeHtml($val) {
	return htmlentities($val);
}