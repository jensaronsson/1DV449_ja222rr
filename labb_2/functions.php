<?php
require_once("get.php");
require_once("add.php");
require_once("sec.php");
sec_session_start();

/*
* It's here all the ajax calls goes
*/ 
if(isset($_GET['function'])) {
	
	if($_GET['function'] == 'logout') {
		logout();
    } elseif($_GET['function'] == 'add') {
       
  	$name = $_GET["name"];
  	$message = $_GET["message"];
  	$pid = $_GET["pid"];
    $token = $_GET["token"];
    // check to see if clienttoken is OK. 
    if(checkToken($token)) {
      addToDB($name, $message, $pid);
    } else {
      die("Det en icke tillåten förfrågan. Token är fel.");

    }
		

    }
    elseif($_GET['function'] == 'producers') {
    	$pid = $_GET["pid"];
   		echo(json_encode(getProducer($pid)));
    }

    elseif($_GET['function'] == 'getMessages') {
      $pid = $_GET["pid"];
      echo(json_encode(getMessages($pid)));
    }
}