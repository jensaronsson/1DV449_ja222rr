# Webbteknik II - labb2 (ja222rr)


## 1. Optimering

> Första laddning av sidan med inloggning gav 20 request och laddtid på 3.17 sek, 2.4mb i resurs.
> Första laddning av sidan efter inloggning gav 18 request och laddtid på 2.46 sek, 2.4mb resurs.


### Åtgärd 1

#### Bortagning av redirect sleep.

__Teori__:

Jag tog bort sleep funktionen på server så gjorde att servern pausade exekveringen i 2sekunder. 
I och med borttagningen av detta så minskade med ett anrop. 
Man ska alltid sträva efter så få HTTP anrop som möjligt.
 
*High performance web sites, Kapitel 1, sida 10.*

__Före__

* Laddtid (Medeltal): 2.11 sek
* Resurs: 2.4mb
* Anrop: 20 st

__Efter__

* Laddtid: (Medeltal): 2.04 sek 
* Resurs: 2.4mb
* Anrop: 19 st

__Vinst__

* Vinsten här mäts i att det fanns en sleep på server på 2 sekunder.
 
__Reflektion__

I detta fall kan jag inte se något nytta med att ha en sleep på servern.
ppenbart att jag skulle få en vinst på samma som sleepen är satt på. 

### Åtgärd 2

#### Kombinera css & javascript filer

__Teori__ :

Kombinera css och script filer för att få ner antal förfrågningar till servern

*[http://developer.yahoo.com/performance/rules.html#num_http](http://developer.yahoo.com/performance/rules.html#num_http)*

__Före__ :

* Laddtid: (Medeltal): 2.04 sek
* Resurs: 2.4mb
* Anrop: 19 st

__Efter__ :

* Laddtid: (Medeltal): 1.9 sek
* Resurs: 2.4mb
* Anrop: 14 st


__Vinst__ :

* Fick ner antal anrop och svarstid till 1.9 sek, resursen är oförändrad.

__Reflektion__ :

Samlade all inline css och javascript kod, samt de inlänkade filerna till en och samma fil och fick därför bort 5 anrop till server.
Det resulterade i att jag fick ner responstiden en hel sekund.


### Åtgärd 3

##### Minifiera css & javascript filer

__Teori__ :

Minifiera skriptfiler och ta bort onödigt utrymme i de inlänkade filerna, borde få ner resursstorleken något.

*High performance web sites, Kapitel 10, sida 69.*

__Före__ :

* Laddtid: (Medeltal): 1.9 sek
* Resurs: 2.4mb
* Anrop: 14 st


__Efter__ :

* Laddtid: (Medeltal): 1.5 sek
* Resurs: 2.2mb
* Anrop: 14 st

__Vinst__ :

* Här fick jag ner laddtiden till 1.5 sek och resurserna minskade till 2.2mb.

__Reflektion__ :

Jag blev förvånad av hur mycket mindre filerna blev efter man minifierat filerna. Men den totala vinsten blev inte så stor. Jag fick ner svarstiden till 1.5 sek vilket jag trodde skulle bli mindre. 



### Åtgärd 4


##### Beskära bilden food

__Teori__ :

Beskär bilden food.jpg till den storleken som den formateras till för att minska filstorleken.

*[https://developers.google.com/speed/docs/best-practices/payload#CompressImages](https://developers.google.com/speed/docs/best-practices/payload#CompressImages)*

__Före__ :

* Laddtid: (Medeltal): 1.5 sek
* Resurs: 2.2mb
* Anrop: 14 st


__Efter__ :

* Laddtid: (Medeltal): 1.4 sek
* Resurs: 282 kb
* Anrop: 14 st

__Vinst__ :

* Vinsten jag fick här var att jag fick ner resurserna med 282kb, man såg även en liten snabbare laddtid.


__Reflektion__ :

Att ha en fullstor bild som skalas ner till liten storlek känns ganska onödig last på applikationen. Att försöka få ner filstorleken känns som ganska common sense.

### Åtgärd 5


##### Skapa en sprite för lightbox relaterade bilder.

__Teori__:

Bunta ihop bilderna för navigering av lightbox. 

*High performance web sites, Kapitel 1, sida 11 - Css Sprites.

__Före__ :

* Laddtid: (Medeltal): 1.4 sek
* Resurs: 282kb
* Anrop: 14 st


__Efter__ :

* Laddtid: (Medeltal): 1.8 sek
* Resurs: 281 kb
* Anrop: 12 st

__Vinst__ :

* Jag minskade http anropen med 2st. Något högre laddtid. Kanske beror på fler som kör mot servern på helgen?

__Reflektion__ :

Ganska så effektiv åtgärd för att ner http anropen om man jobbar med många ikoner och bilder.


### Åtgärd 6

##### Ta bort trasiga länkar och felaktiga förfrågningar

__Teori__ :

Förfrågningar till server som svarar med 404 eller 410 är onödiga förfrågningar och bör uppdateras eller tas bort för att optimera.

*[https://developers.google.com/speed/docs/best-practices/rtt#AvoidBadRequests](https://developers.google.com/speed/docs/best-practices/rtt#AvoidBadRequests)*

__Före__ :

* Laddtid: (Medeltal): 1.8 sek
* Resurs: 281 kb
* Anrop: 12 st


__Efter__ :

* Laddtid: (Medeltal): 2.1 sek
* Resurs: 281 kb
* Anrop: 10 st

__Vinst__ :

* Minskade anrop med 2st men laddtid gick upp. Jag misstänker att css filerna som skrapas på vhost.lnu är högre belastad under helgen då dessa tar nästa 2sek 

__Reflektion__ :

När man har döda länkar eller inte hittar filer så bryter man den parallela inladdningen, vilket medför till att laddtiden tar längre tid.



### Åtgärd 7

#### Färre databasanrop

__Teori__ : 

Färre databasanrop är bra ur en prestandasynpunkt

*[http://stackoverflow.com/questions/2077654/in-php-mysql-should-i-open-multiple-database-connections-or-share-1](http://stackoverflow.com/questions/2077654/in-php-mysql-should-i-open-multiple-database-connections-or-share-1)*


__Reflektion__ : 

Svårt att mäta vinst här då ju fler meddelande det finns per producent desto fler anrop görs till servern. 

Förut så hämtades varje meddelande tillhörande en producent separat. Vilket gjorde att det gjordes ett anrop per meddelande. Nu hämtas alla meddelande tillhörande en producent på ett o samma anrop. Vilket optimerar framförallt när man börjar få många meddelanden och det lätt blir många anrop. 


### Åtgärd 8

#### Spara ned externa css anropt lokalt

Sparade ner externa css filerna som anropades då dessa var den absolut största boven i laddningstid. 


__Före__ :

* Laddtid: (Medeltal): 2.1 sek
* Resurs: 281 kb
* Anrop: 10 st


__Efter__ :

* Laddtid: (Medeltal): 852ms
* Resurs: 395 kb
* Anrop: 9 st

__Vinst__ : 

En otroligt stor vinst i laddtid av att spara hem dessa css filern lokalt.
Dock en liten ökning i resursstorlek.

__Reflektion__ :

Om det inte finns en särskild anledning till att hålla dessa anrop till css filerna externa, så är det givet ur optimerings synpunkt. 


### Övriga åtgärder ( implementerade med ej mätt)

#### 1. Kombinera google fonts förfrågningarna till en förfrågning. 
Man kan bunta ihop anropet till google fonts till ett o samma.





## 2. Säkerhetshål

### 1. SQL injections

#### Säkerhetshål:

Icke parameteriserade frågor möjliggjorde sql-injections till databasen.

#### Risker: 

Man kan man med enkelhet skicka med en förlängning av en sql sats som kan förändra resultet av ursprunglinga sql satsen.

#### Skada: 

En ond användare kan med hjälp av SQL injections se till att alltid bli inloggad eller få ut restrikt data eller radera en hel databas.

#### Åtgärd:

Jag har åtgärdat problemet genom att använda mig av parameter frågor.

På så sätt så kan jag bestämma exakt vart input ska vara i sql frågan, och tar bort risken för att användaren skriver in sql frågor.


### 2. Inloggningen javascript dödar ej sessionen

#### Säkerhetshål:

När användaren loggar ut så dödas ej sessionen, man är fortfarande inloggad.

#### Risker:

Användaren luras till att tro att man är utloggad. Sitter man på en offentlig dator så kan nästa person enkelt logga in.

#### Skada: 

Beroende på vad som för slags inloggning som användaren har rätt till.

#### Åtgärd:

Skapar en postback till servern som tar hand om utloggningen.


### 3. Cross site request forgery

#### Säkerhetshål:

Det finns ingen koll på om det som skickas tillbaka från ett formulär är från en betrodd användare.

#### Risker:

En ond användare kan genom en annan sida, få besökaren att skicka data till applikationen i hemlighet utan att användaren vet detta. Bara genom att användaren besöker den skadliga sidan.

#### Skada: 

Den onda användaren kan använda din cookie för att skicka tillåtna förfrågningar till applikationen. 


#### Åtgärd:

Jag skapar en token som sätts i en session varje gång sidan laddas för formuläret. Token hamnar även i ett hidden input fält för formuläret.
När man postar till servern så kollar jag ifall token från session stämmer överens med token från formuläret.



### 4. Cross Site Script (XSS)

#### Säkerhetshål:

Man kan skjuta in skadlig kod i meddelande funktionen på producentlistan.

#### Risker:

Man kan genom att skriva in javascript i ett meddelande och få det att exekvera när man hämtar ut och visar meddelanden från databasen.

#### Skada: 

Man kan med hjälp av javascript stjäla cookiesessionen genom en skadlig länk.

#### Åtgärd:

Kör en escape på allt som ska in i databasen innan man sparar.




## 3. Ajax


#### Implementation:

I ajaxanropet som körs vid skapandet av ett nytt meddelande så kallar jag på `changeProducer(pid)` som renderar ut meddelandena igen.

Stötte på problem då meddelanden hamnade i fel ordning. 
Detta berodde på att meddelandena hämtades en o en i callback funktioner så det gick inte att garantera ordning.


Därför fick jag skapa en ny funktion på serversidan `function getMessages($pid)`
som tar emot ett producentId. Den frågar sedan databasen om alla meddelanden kopplade till producent id:t, sorterade på skapande datum och i fallande ordning.

Returnerar JSON datat till klienten som renderar ut meddelandena.

javascripten är minifierad i projektet men ajax anropen hittas här
[Länk till javascript.]
(https://github.com/enpere/1DV449_ja222rr/blob/master/labb_2/js/js.js)

[Länk till Körbar Version](http://blogg.onjumpstarter.io/labb2)







