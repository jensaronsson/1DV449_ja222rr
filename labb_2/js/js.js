
     $( document ).ready(          
        function() {
            
            $('#mess_container').hide();
            
            $("#add_btn").bind( "click", function() {
                
                var name_val = $('#name_txt').val();
                var message_val = $('#message_ta').val();
                var pid =  $('#mess_inputs').val();
                var token =  $('#token').val();
                
                $.ajax({
                    type: "GET",
                    url: "functions.php",
                    data: {function: "add", token: token, name: name_val, message: message_val, pid: pid}
                }).done(function(data) {
                    // Rerender messages.
                  changeProducer(pid);
                });
              
          });
        })
    

    function changeProducer(pid) {
            
            
                            
            // Clear and update the hidden stuff
            $( "#mess_inputs").val(pid);
            $( "#mess_p_mess").text("");
            
            // get all the stuff for the producers
            // make ajax call to functions.php with teh data
            $.ajax({
                type: "GET",
                url: "functions.php",
                data: {function: "producers", pid: pid}
            }).done(function(data) { // called when the AJAX call is ready
                
                var j = JSON.parse(data);
                
                $("#mess_p_headline").text("Meddelande till " +j.name +", " +j.city);
                
                
                if(j.url !== "") {
                    
                    $("#mess_p_kontakt").text("Länk till deras hemsida " +j.url);
                }
                else {
                    $("#mess_p_kontakt").text("Producenten har ingen webbsida");
                }
                
                if(j.imageURL !== "") {
                    $("#p_img_link").attr("href", j.imageURL); 
                    $("#p_img").attr("src", j.imageURL); 
                }
                else {
                    $("#p_img_link").attr("href", "#"); 
                    $("#p_img").attr("src", "img/noimg.jpg"); 
                }
            });
            
            // calling getMessage to retrive all messages associated to producersid
            $.ajax({
                type: "GET",
                url: "functions.php",
                data: {function: "getMessages", pid: pid}
                
            }).done(function(data) {
                
                // all the messages for the messages for this producer
                var messages = JSON.parse(data);
                
                
                // Loop through all the messages and print them out
                if(messages !== false){
                 messages.forEach(function(m) {

                     $( "#mess_p_mess" ).append( "<p class='message_container'>" +m.message +"<br />Skrivet av: " +m.name +" | " + m.created_at + "</p>");

                    // // problems with the messages not coming in the right order :/
                    // $.ajax({
                    //     type: "GET",
                    //     url: "functions.php",
                    //     data: {function: "getMessage", serial: entry.serial},
                    //     //timeout: 2000
                    // }).done(function(data) {
                    //     var j = JSON.parse(data);
                    // //  console.log(j);
                        // $( "#mess_p_mess" ).append( "<p class='message_container'>" +j.message +"<br />Skrivet av: " +j.name +"</p>");
                
                    //});
                });
                }
                
            });
            
            // show the div if its unvisible
            $("#mess_container").show("slow");
            
        }